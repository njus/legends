import { Container, Box, Heading, Text } from "@chakra-ui/react";
import theme from "../utils/theme";

type SummaryProps = {
  heading: string;
  content: string;
};

const summary = ({ heading, content }: SummaryProps) => {
  return (
    <Box
      width={{ sm: "full", lg: "container.lg" }}
      m="auto"
      py={[16]}
      px={[4.5, 10]}
    >
      <Box textAlign="center">
        <Heading
          position="relative"
          size="2xl"
          zIndex={1}
          mb={[12, 16]}
          _after={{
            content: "''",
            width: "150px",
            height: "25%",
            position: "absolute",
            bottom: [-6, -8],
            right: "50%",
            transform: "auto",
            translateX: "50%",
            bg: theme.colors.brand["yellow"],
            zIndex: -1,
          }}
        >
          {heading}
        </Heading>
      </Box>
      <Container bgColor="white">
        <Text
          fontSize={["xl", "2xl"]}
          lineHeight="tall"
          color={"gray.500"}
          textAlign="center"
          // px={[2.5, 1, 0]}
        >
          {content}
        </Text>
      </Container>
    </Box>
  );
};

export default summary;
