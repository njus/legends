import { Box, Heading } from "@chakra-ui/react";
import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Autoplay, Pagination, Navigation } from "swiper";
import handShake from "../public/handShake-min.jpg";
import adult from "../public/main1-min.jpg";
import teen from "../public/main3-min.jpg";

// const images = ["/handShake.jpg", "/main1.jpg", "/main3.jpg"];
const images = [handShake, adult, teen];

export default function Slider() {
  return (
    <Box
      position="relative"
      h={["55vh", "80vh"]}
      width="full"
      maxH={["620px", "860px"]}
      _after={{
        content: "''",
        width: "100%",
        height: "100%",
        position: "absolute",
        bottom: 0,
        left: 0,
        bg: "rgba(0,0,0,0.5)",
        zIndex: 1,
      }}
    >
      <Swiper
        spaceBetween={30}
        centeredSlides={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        loop={true}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Autoplay, Pagination, Navigation]}
        className="mySwiper"
      >
        {images.map((img, index) => (
          <SwiperSlide key={index} style={{ width: "100%" }}>
            <Image layout="fill" src={img} alt="slider" />
            <Heading position="absolute" zIndex={-9999} hidden={true} as="h1">
              legendsmaine.care
            </Heading>
          </SwiperSlide>
        ))}
      </Swiper>
    </Box>
  );
}
