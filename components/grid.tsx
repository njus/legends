import { Box, SimpleGrid, Heading } from "@chakra-ui/react";
import Image from "next/image";

const features = [
  {
    text: "We Support",
    image: "/support-min.jpg",
  },
  {
    text: "We Assist",
    image: "/assist-min.jpg",
  },
  {
    text: "We Serve",
    image: "/serve-min.jpg",
  },
  {
    text: "We Care",
    image: "/care-min.jpg",
  },
];

const HomeGrid = () => {
  return (
    <Box
      width={{
        sm: "full",
        lg: "container.lg",
      }}
      px={5}
      pb={[10, 20]}
      mx={{
        md: "auto",
      }}
    >
      <SimpleGrid
        columns={{
          base: 1,
          md: 2,
          lg: 2,
        }}
        spacing={[5, 7]}
      >
        {features.map(({ text, image }) => (
          <Box
            key={text.substring(3)}
            position="relative"
            h={["40vh", "25vh", "30vh"]}
            maxH={["220px", "360px"]}
            borderRadius="2xl"
            overflow="hidden"
          >
            <Image objectFit="cover" layout="fill" src={image} alt="banner" />
            <Box
              position={"relative"}
              display={"flex"}
              h="100%"
              justifyContent="center"
              alignItems={"center"}
              backgroundColor={"rgba(0,0,0,0.4)"}
              borderRadius="2xl"
            >
              <Box color="white" borderRadius="2xl">
                <Heading as="h3" size={"xl"}>
                  {text}
                </Heading>
              </Box>
            </Box>
          </Box>
        ))}
      </SimpleGrid>
    </Box>
  );
};

export default HomeGrid;
