import Link from "next/link";
import {
  Box,
  Image,
  Flex,
  Stack,
  HStack,
  IconButton,
  useDisclosure,
  useColorModeValue,
} from "@chakra-ui/react";
import { HamburgerIcon, CloseIcon } from "@chakra-ui/icons";

const list = ["Home", "About us", "Services", "Contact us"];
const secondaryColor = "#FC6536";

const Header = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <Box py={10}>
      <Flex justify="space-between" align="center">
        <Link href="/">
          <a>
            <Image
              height={["50px", "70px"]}
              align="center"
              objectFit="contain"
              src="/logo.png"
              alt="logo image"
              fallbackSrc="https://via.placeholder.com/170x70"
            />
          </a>
        </Link>
        <IconButton
          size={"md"}
          bgColor={secondaryColor}
          outline="none"
          icon={
            isOpen ? (
              <CloseIcon color="white" />
            ) : (
              <HamburgerIcon color="white" />
            )
          }
          aria-label={"Open Menu"}
          display={{ md: "none" }}
          onClick={isOpen ? onClose : onOpen}
        />
        <HStack spacing={0.5} display={{ base: "none", md: "flex" }}>
          {NavBar({ list, onClose })}
        </HStack>
      </Flex>
      {isOpen ? (
        <Box
          borderRadius="md"
          mt={1.5}
          py={4}
          display={{ md: "none" }}
          bgColor={secondaryColor}
        >
          <Stack spacing={4}>{NavBar({ list, onClose })}</Stack>
        </Box>
      ) : null}
    </Box>
  );
};

const NavBar = ({ list, onClose }: { list: string[]; onClose: any }) => {
  return list.map((item, i) => {
    const link = item.split(" ")[0];
    const href = i === 0 ? "/" : `/#${link.toLocaleLowerCase()}`;
    return (
      <Box
        key={link}
        borderRadius={"sm"}
        fontWeight="semibold"
        bgColor={{ base: "inherit", md: secondaryColor }}
        color={["white", "white"]}
        _hover={{
          bg: [
            useColorModeValue(secondaryColor, "gray.300"),
            useColorModeValue("green.500", "gray.300"),
          ],
          // color: "white",
          // opacity: 0.85,
        }}
      >
        <Link href={href}>
          <a onClick={onClose} style={{ padding: "10px 20px" }}>
            {item}
          </a>
        </Link>
      </Box>
    );
  });
};

export default Header;
