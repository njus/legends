import { ReactElement } from "react";
import { Text, Stack, Flex } from "@chakra-ui/react";
import theme from "../utils/theme";

interface FeatureProps {
  text: string;
  icon: ReactElement;
}

const Feature = ({ text, icon }: FeatureProps) => {
  return (
    <Stack
      bgColor={"gray.100"}
      border={`1px solid ${theme.colors.brand["yellow.border"]}`}
      rounded="lg"
      boxShadow="2xl"
      p={6}
    >
      <Flex
        w={16}
        h={16}
        align={"center"}
        justify={"center"}
        color={"white"}
        rounded={"lg"}
        bg={theme.colors.brand["yellow.icon"]}
        mb={1}
      >
        {icon}
      </Flex>
      <Text fontSize={"lg"} color={"gray.600"}>
        {text}
      </Text>
    </Stack>
  );
};

export default Feature;
