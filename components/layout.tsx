import { ReactNode } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import Image from "next/image";
import {
  Flex,
  Box,
  Container,
  Text,
  Stack,
  HStack,
  Divider,
  IconButton,
  useDisclosure,
  useColorModeValue,
  Heading,
} from "@chakra-ui/react";
import { HamburgerIcon, CloseIcon } from "@chakra-ui/icons";

import Head from "./head";
import theme from "../utils/theme";

const spacing = 0.3;
const list = ["Home", "About us", "Services", "Contact us"];

type Props = {
  children?: ReactNode;
  title?: string;
  pathname?: string;
};

type imageSize = {
  width?: string;
  height?: string;
};

const Layout = ({
  children,
  title = "Legends Residential Care",
  pathname = "/",
}: Props) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const router = useRouter();

  return (
    <Flex direction="column">
      <Head title={title} pathname={pathname} />
      <Container maxW="container.lg" my={1}>
        <Flex justify="space-between" align="center">
          <LogoLink img={"/__logo__w.png"} />
          <IconButton
            size={"md"}
            bgColor={theme.colors.brand["brown.200"]}
            outline="none"
            icon={
              isOpen ? (
                <CloseIcon color="white" />
              ) : (
                <HamburgerIcon color="white" />
              )
            }
            aria-label={"Open Menu"}
            display={{ md: "none" }}
            onClick={isOpen ? onClose : onOpen}
          />
          <HStack spacing={0.5} display={{ base: "none", md: "flex" }}>
            {NavBar({ list, pathname: router.pathname, onClose })}
          </HStack>
        </Flex>
        {isOpen ? (
          <Box
            borderRadius="md"
            mt={1.5}
            py={4}
            px={2}
            display={{ md: "none" }}
            bgColor={"inherit"}
          >
            <Stack spacing={4}>
              <FootLinks spacing={2.5} />
            </Stack>
          </Box>
        ) : null}
      </Container>
      {children}

      {/* footer */}
      <Box
        px={[2.5, 1, 0]}
        bgColor={theme.colors.brand["brown.200"]}
        color="gray.300"
      >
        <Container maxW="container.lg" py={["60px", "80px"]}>
          <Flex justify="space-between" direction={["column", "row"]}>
            <Flex direction="column" flex={0.5} ml={-2}>
              <LogoLink
                img={"/__logo__b.png"}
                size={{ height: "90px", width: "225.68px" }}
              />
              <Box pl={2} pb={4} opacity={0.7}>
                <Text>324 Cummings Rd, Unit 101</Text>
                <Text>South Portland, ME 04106</Text>
              </Box>
            </Flex>
            <FootLinks spacing={10} />
            <Flex direction="column" flex={0.25} opacity={0.7} pt={[10, 0]}>
              <Heading as="h4" pb={4} size="md">
                Operating hours
              </Heading>
              <Box>
                <Flex
                  justify="space-between"
                  py={["8px", "6px"]}
                  px="5px"
                  borderBottom="1px"
                  borderBottomColor={theme.colors.brand["yellow.200"]}
                >
                  <Text fontWeight="semibold">Services hours</Text>
                  <Text fontSize="sm">24/7</Text>
                </Flex>
                <Box py="20px">
                  <Text pb="8px" fontWeight="semibold">
                    Office hours
                  </Text>
                  <Flex
                    justify="space-between"
                    py={["8px", "6px"]}
                    px="5px"
                    fontSize="sm"
                    borderBottom="1px"
                    borderBottomColor={theme.colors.brand["yellow.200"]}
                  >
                    <Text>Mon - Fri</Text>
                    <Text>9am - 5pm</Text>
                  </Flex>
                  <Flex
                    justify="space-between"
                    py={["8px", "6px"]}
                    px="5px"
                    fontSize="sm"
                    borderBottom="1px"
                    borderBottomColor={theme.colors.brand["yellow.200"]}
                  >
                    <Text>Sat - Sun</Text>
                    <Text>Closed</Text>
                  </Flex>
                </Box>
              </Box>
            </Flex>
          </Flex>
        </Container>
        <Divider orientation="horizontal" opacity={0.1} />
        <Box
          py="20px"
          color={["gray.400", "gray.400"]}
          textAlign="center"
          display={["block", "flex"]}
          justifyContent={["center"]}
          opacity="0.5"
          fontSize={"sm"}
        >
          <Text as="span" size="sm">
            &copy;{new Date().getFullYear()}&nbsp;
            <b>Legends Residential Care LLC.</b>
          </Text>
          <Text>&nbsp;All Rights Reserved.</Text>
        </Box>
      </Box>
    </Flex>
  );
};

function LogoLink(
  { img, size = { height: "60px", width: "152.17px" } },
  { img: string, size: imageSize }
) {
  return (
    <Link href="/">
      <a style={size}>
        <Image
          height={size.height}
          width={size.width}
          objectFit="contain"
          src={img}
          alt="logo"
        />
      </a>
    </Link>
  );
}

const NavBar = ({
  list,
  pathname,
  onClose,
}: {
  list: string[];
  pathname?: string;
  onClose?: any;
}) => {
  return list.map((item, i) => {
    const link = item.split(" ")[0];
    const href = i === 0 ? "/" : `/${link.toLocaleLowerCase()}`;
    const router = useRouter();
    const color =
      router.pathname === href
        ? theme.colors.brand["yellow.50"]
        : theme.colors.brand["footer"];
    return (
      <Box
        key={link}
        position="relative"
        borderRadius={"sm"}
        fontWeight="semibold"
        bgColor={{ base: "inherit" }}
        // color={color}
        textAlign="center"
        // mx="10px"
        zIndex={1}
        // _after={{
        //   content: "''",
        //   position: "absolute",
        //   backgroundColor: theme.colors.brand.yellow,
        //   height: "7px",
        //   width: "full",
        //   left: 0,
        //   bottom: "3px",
        //   transition: ".3s",
        //   zIndex: -1,
        // }}
      >
        <Link href={href}>
          <a className="navlink" style={{ color }} onClick={onClose}>
            {item.toUpperCase()}
          </a>
        </Link>
      </Box>
    );
  });
};

function FootLinks({ spacing: string }) {
  return (
    <Flex
      py={[spacing, 0]}
      mt={[5, 0]}
      direction="column"
      flex={[1, 0.3]}
      opacity={0.7}
    >
      <Heading as="h4" pb={4} size="md">
        Menu
      </Heading>
      {list.map((item, i) => {
        const link = item.split(" ")[0];
        const href = i === 0 ? "/" : `/${link.toLocaleLowerCase()}`;
        return (
          <Box
            key={i}
            py={1}
            bg="inherit"
            _hover={{ textDecoration: "underline" }}
          >
            <Link href={href}>
              <a>{item}</a>
            </Link>
          </Box>
        );
      })}
    </Flex>
  );
}

export default Layout;
