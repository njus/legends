import { ReactElement } from "react";
import { Box, Heading } from "@chakra-ui/react";
import Image, { StaticImageData } from "next/image";
import { Children } from "react";
import theme from "../utils/theme";

type BannerProps = {
  title?: string;
  image: string;
  children?: ReactElement;
};
// const bgColor = "rgba(240,184,101,0.2)";
// const bgColor = "rgba(255,255,255,0.4)";
const bgColor = "rgba(0,0,0,0.4)";

const Banner = ({ title, image, children }: BannerProps) => {
  return (
    <Box
      position="relative"
      h={["60vh", "50vh"]}
      width="full"
      maxH={["260px", "460px"]}
    >
      <Image objectFit="cover" layout="fill" src={image} alt="banner" />
      <Box
        position={"relative"}
        display={"flex"}
        h="100%"
        justifyContent="center"
        alignItems={"center"}
        backgroundColor={bgColor}
      >
        <Box color="white">
          <Heading
            size={"3xl"}
            position={"relative"}
            zIndex={1}
            _after={{
              content: "''",
              width: "full",
              height: "35%",
              position: "absolute",
              bottom: 0,
              left: 0,
              bg: theme.colors.brand["yellow"],
              zIndex: -1,
            }}
          >
            {title}
          </Heading>
          {children}
        </Box>
      </Box>
    </Box>
  );
};

export default Banner;
