import Head from "next/head";

type props = {
  title?: string;
  pathname: string;
};

const HeadBox = ({ title, pathname }: props) => {
  return (
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="icon" href="/favicon.ico" key="favicon" />
      <link
        rel="apple-touch-icon"
        sizes="180x180"
        href="/apple-touch-icon.png"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="32x32"
        href="/favicon-32x32.png"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="16x16"
        href="/favicon-16x16.png"
      />
      <link rel="manifest" href="/site.webmanifest" />
      <link
        rel="canonical"
        href={`https://legendsmaine.care${pathname}`}
        key="canonical"
      />
      <meta name="title" content={title} />
      <meta
        name="keywords"
        content="legendsmaine.care, legendsmaine care, legendsmaine, behavioral, home-based support, intellectual disabilities, legends care, legends, development disabilities, legends residential care, legends residential"
      />
      <meta
        key="description"
        name="description"
        content="Legends Residential Care is an organization that supports individuals with developemental disabilities and intellectual disabilities throughout the state of Maine in the United States."
      />
    </Head>
  );
};

export default HeadBox;
