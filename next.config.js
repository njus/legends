const path = require("path");
const isProd = process.env.NODE_ENV === "production";

module.exports = {
  poweredByHeader: false,
  reactStrickMode: true,

  // use the domain in production and localhost for development
  assetPrefix: isProd ? "https://legendsmaine.care" : "",
  images: {
    domains: ["https://legendsmaine.care", "https://ahadi.vercel.app"],
    deviceSizes: [370, 640, 750, 828, 1080, 1200, 1920, 2048, 3840],
  },
  i18n: {
    locales: ["en", "fr", "es"],
    defaultLocale: "en",
  },
};
