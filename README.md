# README

This application is built with [NextJS](https://nextjs.org) and [Chakra UI](https://chakra-ui.com).

### Getting started

To get started, you will need to have some knowledge of version control system. If you are on an apple computer or a linux based computer, [git](https://git-scm.com) comes already installed. On the other hand if you are on Windows system you will need to install [git](https://git-scm.com/download/win). Here's a [cheat sheet](https://training.github.com/downloads/github-git-cheat-sheet/) to get you started with.

- First lets download the [Visual Studio Code IDE](https://code.visualstudio.com/download)

- Clone(copy) the repository to local computer

```bash
git clone git@bitbucket.org:njus/legends.git
```

- Change to the cloned directory

```bash
cd legends
```

- Install packages

```bash
yarn install
```

- Run the application in development mode

```bash
yarn dev
```
