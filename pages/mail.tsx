import { Button, Center } from "@chakra-ui/react";
import { MdEmail } from "react-icons/md";
import Head from "../components/head";
import Layout from "../components/layout";
import Banner from "../components/banner";
import theme from "../utils/theme";

const title = "MailBox";
const url =
  "https://accounts.zoho.com/signin?servicename=VirtualOffice&signupurl=https://www.zoho.com/mail/zohomail-pricing.html&serviceurl=https://mail.zoho.com";

const MailPage = () => {
  const handleClick = () => {
    window.open(url);
  };

  return (
    <Layout title={`${title} | Legends Residential Care`} pathname="/mail">
      <Banner title={title} image={"/mail1920.jpg"}>
        <Center mt={10}>
          <Button
            onClick={handleClick}
            leftIcon={<MdEmail />}
            colorScheme="orange"
            variant={"solid"}
            size={"lg"}
          >
            MailBox Login
          </Button>
        </Center>
      </Banner>
    </Layout>
  );
};

export default MailPage;
