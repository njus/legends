import { Box, Heading, SimpleGrid, Text, Icon } from "@chakra-ui/react";
import Layout from "../components/layout";
import Banner from "../components/banner";
import Summary from "../components/summary";
import theme from "../utils/theme";

const title = "About us";
const heading = "Who we are";
const content =
  "Legends Residential Care, LLC is an organization that supports individuals with developemental disabilities and intellectual disabilities throughout the state of Maine in the United States.";
const values = [
  {
    name: "Integrity",
    icon: "",
  },
  {
    name: "Commitment",
    icon: "",
  },
  {
    name: "Respect",
    icon: "",
  },
  {
    name: "Joy",
    icon: "",
  },
  {
    name: "Compassion",
    icon: "",
  },
  {
    name: "Quality",
    icon: "",
  },
];

const AboutPage = () => {
  return (
    <Layout title={`${title} | Legends Residential Care`} pathname="/about">
      <Banner title={title} image={"/about1920-min.jpg"} />
      <Summary heading={heading} content={content} />
      <Box
        width={{ sm: "full", lg: "container.lg" }}
        px={5}
        pb={[10, 20]}
        mx={{ md: "auto" }}
      >
        <Heading as="h3" size="xl" pb="6" textAlign={"center"}>
          Our core values are
        </Heading>
        <SimpleGrid columns={{ base: 2, md: 3, lg: 3 }} spacing={[3]}>
          {values.map(({ name, icon }) => (
            <Box
              key={name}
              overflow="hidden"
              display={"flex"}
              justifyContent="center"
              alignItems={"center"}
              border="1px"
              borderColor={"gray.200"}
              bgColor="gray.100"
              borderRadius="md"
              shadow={"4xl"}
              h={[12, 15, 20]}
            >
              <Text fontSize={"lg"} fontWeight={500} color="gray.600">
                {name}
              </Text>
            </Box>
          ))}
        </SimpleGrid>
      </Box>
    </Layout>
  );
};

export default AboutPage;
