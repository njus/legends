import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import "../styles/globals.css";
import "../styles/swiper.css";
import theme from "../utils/theme";

function App({ Component, pageProps }) {
  return (
    <ChakraProvider theme={theme}>
      <Component {...pageProps} />
    </ChakraProvider>
  );
}

export default App;
