import {
  Box,
  Container,
  Heading,
  Text,
  SimpleGrid,
  Icon,
} from "@chakra-ui/react";
import {
  FcPortraitMode,
  FcSafe,
  FcConferenceCall,
  FcApproval,
  FcAlarmClock,
  FcSettings,
} from "react-icons/fc";
import Layout from "../components/layout";
import Banner from "../components/banner";
import Summary from "../components/summary";
import Feature from "../components/feature";

const title = "Services";
const heading = "Who we serve";
const content =
  "We support individuals who have been diagnosed with developmental disabilities. Some of the conditions designosed include Autism spectrum disorder, intellectual disability, cerebral palsy.";

const features = [
  {
    text: "Self-care",
    icon: FcApproval,
  },
  {
    text: "Independent living skills and self-management skills",
    icon: FcSettings,
  },
  {
    text: "Medication administration",
    icon: FcAlarmClock,
  },
  {
    text: "Interpersonal skills",
    icon: FcPortraitMode,
  },
  {
    text: "Safety skills",
    icon: FcSafe,
  },
  {
    text: "Communication skills",
    icon: FcConferenceCall,
  },
];

const ServicesPage = () => {
  return (
    <Layout title={`${title} | Legends Residential Care`} pathname="/services">
      <Banner title={title} image={"/services1920-min.jpg"} />
      <Summary heading={heading} content={content} />
      <Box
        w={{ sm: "full", xl: "container.xl" }}
        mx={{ sm: 0, xl: "auto" }}
        mb={{ sm: 0, xl: 20 }}
        px={[2.5, 1, 0]}
        py={[16, 20]}
        backgroundColor="orange.300"
        rounded={{ sm: "none", md: "none", xl: "3xl" }}
      >
        <Box width={{ sm: "full", lg: "container.lg" }} m="auto" px={5}>
          <Text
            fontSize={["xl", "3xl"]}
            fontWeight={500}
            pb={6}
            color={"gray.50"}
          >
            We provide the following services
          </Text>
          <SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing={5} pb={5}>
            {features.map(({ text, icon }, i) => (
              <Feature
                key={i}
                icon={<Icon as={icon} w={8} h={8} />}
                text={text}
              />
            ))}
          </SimpleGrid>
        </Box>
      </Box>
    </Layout>
  );
};

export default ServicesPage;
