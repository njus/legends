import {
  Box,
  Flex,
  Text,
  IconButton,
  Button,
  ButtonGroup,
  VStack,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputLeftElement,
  Textarea,
} from "@chakra-ui/react";
import { useState, useRef } from "react";
import { MdPhone, MdEmail, MdLocationOn, MdOutlineEmail } from "react-icons/md";
import { BsPerson } from "react-icons/bs";
import emailjs from "@emailjs/browser";

import Layout from "../components/layout";
import Banner from "../components/banner";
import theme from "../utils/theme";

const title = "Contact us";
const defaultMessage = {
  name: "",
  email: "",
  text: "",
};

const contacts = [
  { location: "Southern Maine", phone: "(207) 810-6610" },
  { location: "Lewiston & Aubun areas", phone: "(207) 705-1533" },
  { location: "Bangor area", phone: "(207) 401-9888" },
];

const ContactPage = () => {
  const [message, setMessage] = useState({ ...defaultMessage });
  const { name, email, text } = message;
  const contactForm = useRef(null);

  function handleChange(e) {
    const { name, value } = e.target;
    setMessage({ ...message, [name]: value });
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (message === defaultMessage) return;
    emailjs
      .sendForm(
        "EMAILJS_SERVICE_ID",
        "EMAILJS_TEMPLATE_ID",
        contactForm.current,
        "YOUR_USER_ID"
      )
      .then(
        (result) => {
          console.log(result.text);
        },
        (error) => {
          console.log(error.text);
        }
      );
    setMessage(defaultMessage);
  }

  return (
    <Layout title={`${title} | Legends Residential Care`} pathname="/contact">
      <Banner title={title} image={"/contact1920-min.jpg"} />
      <Box
        backgroundColor={"grey.300"}
        m={["0", "auto"]}
        px={6}
        pt={{ sm: "0px", md: "60px", lg: "80px" }}
        pb={{ sm: "0px", md: "40px", lg: "50px" }}
        width={{ sm: "100%", md: "100%", lg: "container.lg" }}
        color="gray.500"
      >
        <Text
          fontSize="md"
          fontWeight="bold"
          px={{ lg: "25px" }}
          py={["25px", 4]}
        >
          After hours and on call contacts
        </Text>
        <Box
          className="contact"
          display={"flex"}
          flexDirection={["column", "row"]}
          justifyContent={["flex-start", "space-between"]}
          alignItems={["flex-start"]}
          px={{ lg: "25px" }}
        >
          {contacts.map(({ location, phone }) => {
            return (
              <VStack key={phone} alignItems="flex-start" flex={1}>
                <ButtonGroup
                  size="sm"
                  isAttached
                  variant="link"
                  borderRadius={"none"}
                >
                  <IconButton
                    aria-label="Add to friends"
                    icon={<MdLocationOn />}
                    color={theme.colors.brand["yellow"]}
                    size="16px"
                    bgColor={"transparent"}
                    // px={1}
                    // borderWidth={1}
                    // borderColor={theme.colors.brand["yellow"]}
                    // borderRadius={"50%"}
                    pointerEvents="none"
                  />
                  <Button
                    ml="5px"
                    fontSize={"sm"}
                    fontWeight="medium"
                    // color={theme.colors.brand["yellow"]}
                    textAlign="left"
                    textTransform={"uppercase"}
                    borderRadius={"none"}
                    pointerEvents="none"
                  >
                    {location}
                  </Button>
                </ButtonGroup>
                <ButtonGroup
                  size="sm"
                  isAttached
                  variant="link"
                  style={{
                    marginTop: "5px",
                    marginBottom: "15px",
                  }}
                >
                  <IconButton
                    aria-label="Add to friends"
                    icon={<MdPhone />}
                    color={theme.colors.brand["yellow"]}
                    size="16px"
                    bgColor={"transparent"}
                    // px={1}
                    // borderWidth={1}
                    // borderColor={theme.colors.brand["yellow"]}
                    // borderRadius={"50%"}
                    pointerEvents="none"
                  />
                  <a href={`tel:${phone}`}>
                    <Button
                      ml="5px"
                      fontSize={"sm"}
                      fontWeight="medium"
                      variant="link"
                    >
                      {phone}
                    </Button>
                  </a>
                </ButtonGroup>
              </VStack>
            );
          })}
        </Box>
      </Box>
      <Box
        className="contact"
        display={"flex"}
        flexDirection={{
          xs: "column",
          md: "column",
          lg: "row",
        }}
        alignItems={{ sm: "center", md: "flex-start", lg: "flex-start" }}
        backgroundColor={"grey.300"}
        width={{ sm: "100%", md: "100%", lg: "container.lg" }}
        m="auto"
        pb={{ sm: "0px", md: "60px", lg: "80px" }}
        px={6}
        color="gray.500"
      >
        <Box
          px={{ lg: "25px" }}
          py={["25px", 0]}
          w={{ xs: "full", md: "full", lg: "max-content" }}
        >
          <Text fontSize="lg">
            Contact us by filling out this form or reach us directly by phone or
            email.
          </Text>
          <VStack
            pl={0}
            py={["20px", "25px"]}
            px={[0]}
            spacing={3}
            alignItems="flex-start"
            borderRadius={"none"}
          >
            <ButtonGroup size="sm" isAttached variant="link">
              <IconButton
                aria-label="Add to friends"
                icon={<MdPhone />}
                color={theme.colors.brand["yellow"]}
                size="20px"
                bgColor={"gray.100"}
                p={1.5}
                borderRadius={"sm"}
                pointerEvents="none"
              />
              <a href="tel: 207-331-7745">
                <Button ml="7px" fontSize={"md"} variant="link">
                  (207) 331-7745
                </Button>
              </a>
            </ButtonGroup>
            <ButtonGroup size="sm" isAttached variant="link">
              <IconButton
                aria-label="Add to friends"
                icon={<MdEmail />}
                color={theme.colors.brand["yellow"]}
                size="20px"
                bgColor={"gray.100"}
                p={1.5}
                borderRadius={"sm"}
                pointerEvents="none"
              />
              <a href="mailto:info@legendsmaine.care">
                <Button ml="7px" fontSize={"md"} variant="link">
                  info@legendsmaine.care
                </Button>
              </a>
            </ButtonGroup>
            <ButtonGroup
              size="sm"
              isAttached
              variant="link"
              borderRadius={"none"}
            >
              <IconButton
                aria-label="Add to friends"
                icon={<MdLocationOn />}
                color={theme.colors.brand["yellow"]}
                size="20px"
                bgColor={"gray.100"}
                p={1.5}
                borderRadius={"sm"}
                pointerEvents="none"
              />
              <Button
                ml="7px"
                fontSize={"md"}
                textAlign="left"
                borderRadius={"none"}
                pointerEvents="none"
              >
                324 Cummings Rd, Unit 101
                <br /> South Portland, ME 04106
              </Button>
            </ButtonGroup>
          </VStack>
        </Box>
        <Box display={["block"]} bg="white" w={"100%"} pb={["55px", 0]}>
          <form ref={contactForm} style={{ width: "100% !important" }}>
            <VStack spacing={3.5} flexGrow={{ sm: 0, md: 0, lg: 1 }}>
              <FormControl id="name">
                <FormLabel mb={1}>Name</FormLabel>
                <InputGroup borderColor="#E0E1E7">
                  <InputLeftElement
                    pointerEvents="none"
                    children={<BsPerson color="gray.800" />}
                  />
                  <Input
                    onChange={handleChange}
                    type="text"
                    size="md"
                    name="name"
                    value={name}
                  />
                </InputGroup>
              </FormControl>
              <FormControl id="email">
                <FormLabel mb={1}>Email</FormLabel>
                <InputGroup borderColor="#E0E1E7">
                  <InputLeftElement
                    pointerEvents="none"
                    children={<MdOutlineEmail color="gray.800" />}
                  />
                  <Input
                    formNoValidate={false}
                    onChange={handleChange}
                    type="text"
                    size="md"
                    name="email"
                    value={email}
                  />
                </InputGroup>
              </FormControl>
              <FormControl id="message">
                <FormLabel mb={1}>Message</FormLabel>
                <Textarea
                  onChange={handleChange}
                  borderColor="gray.300"
                  placeholder="message"
                  outline={"0px"}
                  name="text"
                  value={text}
                />
              </FormControl>
              <FormControl id="name" float="right">
                <Button
                  onClick={handleSubmit}
                  variant="solid"
                  bg={theme.colors.brand["yellow"]}
                  color="white"
                  outline={"none"}
                  _hover={{
                    outline: "none",
                    outlineWidth: "0px",
                    bgColor: theme.colors.brand["yellow.100"],
                  }}
                >
                  Send Message
                </Button>
              </FormControl>
            </VStack>
          </form>
        </Box>
      </Box>
      <Box height={["300px", "500px"]}>
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2887.2553324940563!2d-70.35666748407974!3d43.64285577912163!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cb2999bdf8b6a27%3A0xe36c64006a5f6e44!2s324%20Cummings%20Rd%2C%20South%20Portland%2C%20ME%2004106!5e0!3m2!1sen!2sus!4v1667974391472!5m2!1sen!2sus"
          title="Legends Residential Care"
          height={"100%"}
          width={"100%"}
          loading="lazy"
        />
      </Box>
    </Layout>
  );
};

export default ContactPage;
