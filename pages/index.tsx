import HomeGrid from "./../components/grid";
import Layout from "../components/layout";
import Slider from "../components/slider";
import Summary from "../components/summary";

const heading = "What we believe";
const content =
  "At Legends Residential Care, we believe that individuals with intellectual disabilities and/or autism can reach their own God given potiential. Our goal is to support them in developing skills that enable them to be independent.";

const HomePage = () => {
  return (
    <Layout title="Legends Residential Care" pathname="/">
      <Slider />
      <Summary heading={heading} content={content} />
      <HomeGrid />
    </Layout>
  );
};

export default HomePage;
