import { Button, Center } from "@chakra-ui/react";
import { MdLock } from "react-icons/md";
import Layout from "../components/layout";
import Banner from "../components/banner";

const title = "Dashboard";
const url =
  "https://accounts.zoho.com/signin?servicename=VirtualOffice&signupurl=https://www.zoho.com/mail/zohomail-pricing.html&serviceurl=https://mailadmin.zoho.com";

const AdminMailPage = () => {
  const handleClick = () => {
    window.open(url);
  };

  return (
    <Layout title={`${title} | Legends Residential Care`} pathname="/dashboard">
      <Banner title={title} image={"/dashboard1920.jpg"}>
        <Center mt={10}>
          <Button
            onClick={handleClick}
            leftIcon={<MdLock />}
            colorScheme="orange"
            variant="solid"
            size={"lg"}
          >
            Admin Mail Login
          </Button>
        </Center>
      </Banner>
    </Layout>
  );
};

export default AdminMailPage;
