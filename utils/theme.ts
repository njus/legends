import { extendTheme, ThemeConfig } from "@chakra-ui/react";

const config: ThemeConfig = {
  initialColorMode: "light",
  useSystemColorMode: false,
};

const colors = {
  brand: {
    secondary: "#2D3748", // gray 700
    primary: "#ECC94B", // yellow 400
    orange: "#DD6B20", // orange 500
    "brown.100": "#6c401d",
    yellow: "#f0b865",
    "yellow.200": "#806031",
    "yellow.100": "#e8a135",
    "yellow.50": "#eaaf56",
    "yellow.border": "#e8cb6180",
    "yellow.icon": "#f3e9c7",
    // footer: "#230e00",
    footer: "#372215",
    "brown.200": "#6C401D",
  },
};

const theme = extendTheme({ config, colors });

export default theme;
